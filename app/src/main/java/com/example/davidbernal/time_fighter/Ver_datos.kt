package com.example.davidbernal.time_fighter

import android.app.Activity
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import com.google.firebase.database.FirebaseDatabase

class Ver_datos : Activity() {

    lateinit var listRecyclerView: RecyclerView;
    val dataBase = FirebaseDatabase.getInstance();
    val ref = dataBase.getReference("time-fighter");

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_ver_datos)


        listRecyclerView = findViewById(R.id.recycler_view_22);
        listRecyclerView.layoutManager = LinearLayoutManager(this);
        listRecyclerView.adapter = Ver_datos_Adapter(this, ref);



    }
}
