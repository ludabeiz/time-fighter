package com.example.davidbernal.time_fighter

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.Toast
import com.google.firebase.database.ChildEventListener
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.DatabaseReference


class Ver_datos_Adapter(context:Context,ref:DatabaseReference)
    : RecyclerView.Adapter<VerDatosHolder>() {


    var todoLists:ArrayList<TodoList> = arrayListOf();

    init {
        ref.addChildEventListener(object: ChildEventListener{
            override fun onCancelled(item: DatabaseError) {

            }

            override fun onChildMoved(item: DataSnapshot, p1: String?) {

            }

            override fun onChildChanged(item: DataSnapshot, p1: String?) {

            }

            override fun onChildAdded(item: DataSnapshot, p1: String?) {
                val listTitle = item.child("List").value.toString();
                val listId = item.key.toString();
                todoLists.add(TodoList(listId, listTitle));
                //notifyDataSetChanged();
                notifyItemInserted(todoLists.size)
            }

            override fun onChildRemoved(item: DataSnapshot) {
                val deletedIndex = todoLists.indexOfFirst { it.id == item.key };        //(element -> element.id = item.key)
                Toast.makeText(context,deletedIndex.toString(), Toast.LENGTH_LONG).show();
                todoLists.removeAt(deletedIndex);
                notifyItemRemoved(deletedIndex);
            }

        })
    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int): VerDatosHolder {


        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.list_selection_view_holder, parent, false)

        return VerDatosHolder(view)

    }

    override fun getItemCount(): Int {
        return todoLists.count()
    }


    override fun onBindViewHolder(
        holder: VerDatosHolder,
        position: Int) {

        holder.listTitle.text = todoLists[position].listName;

    }
}