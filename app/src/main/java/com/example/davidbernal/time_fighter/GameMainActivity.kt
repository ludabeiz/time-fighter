package com.example.davidbernal.time_fighter

import android.content.Intent
import android.content.IntentSender
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.os.CountDownTimer//J
import android.os.PersistableBundle
import android.util.Log
import android.support.annotation.IntegerRes
import android.support.v7.app.ActionBar
import android.support.v7.app.AlertDialog
import android.view.LayoutInflater
import android.widget.*
import com.google.firebase.database.*
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.ingreso_nombre.*
import kotlinx.android.synthetic.main.ingreso_nombre.view.*
import java.util.*


class GameMainActivity : AppCompatActivity(){

    val database = FirebaseDatabase.getInstance();
    val ref = database.getReference("time-fighter");

    internal lateinit var gameScoreTextView: TextView //variables que representen a las vistas que tengo en mi layout.
    internal lateinit var timeLeftTextView: TextView
    internal lateinit var tapMeButton: Button
    internal lateinit var scoreButton: Button
    internal var score = 0


//variables para el timeleft //cuando son variables en kotlin se pone Var cuando son constantes se pone val

    internal var gameStarted=false //J almacenar el estado del juego, si empezamos o no el juego
    internal lateinit var countDownTimer: CountDownTimer //agregando la variable para el timer J //objeto que viene en el SO nos permite trabajar de 0 a 60
    internal var countDownInterval=1000L
    internal val initialCountDown = 15000L//J esto es en mili segundos y l es de long //cuando son constantes VAL /sacar la duracion del tiempo = 60 segundos
    internal var timeLeft=15 //j usado para imprimir en pantalla

    internal val  TAG =GameMainActivity::class.java.simpleName// L29almacenar el nombre en la clase en la que estiy



    ////##############################################################///

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

  //      val ref = database.getReference("time-fighter");
//        gameList = mutableListOf()

        Log.d(TAG,  "OnCreate called. Score is $score") //L29

        //connect view to variables
        gameScoreTextView = findViewById(R.id.game_score_text_view)
        timeLeftTextView = findViewById(R.id.time_left_text_view)
        tapMeButton = findViewById<Button>(R.id.tap_me_button)
        scoreButton = findViewById<Button>(R.id.ver_score_button)
    // listView=findViewById(R.id.list_view_mostrar)


        tapMeButton.setOnClickListener { _ -> incrementScore() } //cuando se de clic incremente el score //el guion bajo no es nada pero hay va una variable que es la referencia del boton

        //lunes 29
        if (savedInstanceState != null){
            score=savedInstanceState.getInt(SCORE_KEY)
            timeLeft=savedInstanceState.getInt(TIME_LEFT_KEY)
            restoreGame()
        }else{
            resetGame()
        }


        scoreButton.setOnClickListener {
            mostrarDatosBase()
        }
    }

    //MANUALMENTE GUARDAR EL ESTADO DE MI ACTIVITY (Lunes 29)
    companion object {
        private val SCORE_KEY = "SCORE KEY"
        private val TIME_LEFT_KEY = "TIME_LEFT_KEY"
    }

    private fun guardarEnBase(score: Int){

        //poner los item de ingreso
        val dialogoIngresoNombre = LayoutInflater.from(this).inflate(R.layout.ingreso_nombre,null);
        //alertDialogBuilder
        val dialogbuilder=AlertDialog.Builder(this)
            .setView(dialogoIngresoNombre)
            .setTitle("Enter Name")
        //show dialog
        val alertDialogver=dialogbuilder.show()
        //poner el boton
        dialogoIngresoNombre.guardarDatos.setOnClickListener {

            //obtener texto de los estitext
            val nombre=dialogoIngresoNombre.nombreIngreso.text.toString()

            ref.child(UUID.randomUUID().toString()).child("List").setValue(nombre+ "Score:"+score)

            alertDialogver.dismiss()
        }
    }

    private fun mostrarDatosBase(){

        /*
       //poner los item de ingreso
        val dialogoMostrarDatos = LayoutInflater.from(this).inflate(R.layout.ver_datos,null);
        //alertDialogBuilder
        val dialogbuilderVerDatos=AlertDialog.Builder(this)
            .setView(dialogoMostrarDatos)
            .setTitle("Score List")
        //show dialog
        val alertDialogMostrarDatos=dialogbuilderVerDatos.show()
        //poner el boton
*/
        val intent = Intent(this,Ver_datos::class.java);
        startActivity(intent);
    }

    //J funcion para inicializar todas las cosas y el timer , es decir setear todos a sus valores iniciales
    private fun resetGame(){
        score=0
        timeLeft=15

        val gameScore = getString(R.string.your_score, Integer.toString(score))
        gameScoreTextView.text=gameScore

        val timeLeftText = getString(R.string.time_left, Integer.toString(timeLeft))
        timeLeftTextView.text= timeLeftText

        countDownTimer = object : CountDownTimer(initialCountDown, countDownInterval) {
            //inicializando un objeto de tipo countDownTimer
            override fun onTick(millisUntilFinished: Long) { //59,58,57,56
                timeLeft = millisUntilFinished.toInt()/1000
                timeLeftTextView.text = getString(R.string.time_left, Integer.toString(timeLeft))
            }
            override fun onFinish() {
                endGame()
            }
        }
        gameStarted=false
    }
//####################################################################//

    private fun startGame(){
        countDownTimer.start()
        gameStarted = true
    }

    private fun endGame(){
        Toast.makeText(this, getString(R.string.game_over_message, Integer.toString(score)), Toast.LENGTH_LONG).show()
        val scoreBase=score

        guardarEnBase(scoreBase)
        resetGame()
    }


    private fun incrementScore() {
        //gameStarted = true    //J el estado del juego cambia cuando le damos primer tap al boton
        score++
        //val newScore= "Your Score: "+ Integer.toString(score) //aqui estabamos quemando la variable
        val newScore = getString(R.string.your_score, Integer.toString(score))
        gameScoreTextView.text = newScore


        if (!gameStarted) {
            startGame()
        }
    }

//Lunes 29

    private fun restoreGame(){
        val restoredScore= getString(R.string.your_score,Integer.toString(score))
        gameScoreTextView.text=restoredScore

        val restoredTime=getString(R.string.time_left,Integer.toString(timeLeft))
        timeLeftTextView.text=restoredTime


        countDownTimer= object : CountDownTimer(timeLeft *1000L, countDownInterval){
            override fun onTick(millisUntilFinished: Long) {
                timeLeft= millisUntilFinished.toInt()/1000

                timeLeftTextView.text=getString(R.string.time_left, timeLeft.toString())
                //timeLeftTextView.text = Integer.toString(timeLeft)

            }
            override fun onFinish() {
                endGame()
            }
        }
        countDownTimer.start()
        gameStarted=true
    }
    override fun onSaveInstanceState(outState: Bundle?) {
        super.onSaveInstanceState(outState)

        outState!!.putInt(SCORE_KEY,score) //guardando el estado
        outState!!.putInt(TIME_LEFT_KEY,timeLeft) //guardando el estado
        countDownTimer.cancel()

        Log.d(TAG,"onSaveInstanceState : score = $score & timeLeft: $timeLeft")
    }

    override fun onDestroy() {
        super.onDestroy()
        Log.d(TAG,"onDestroy called")
    }

}
